import React, { Component } from 'react';

import './scss/App.css';
import BidPanel from './components/BidPanel';
import PlayerCards from './components/PlayerCards';
import OtherPlayerHand from './components/OtherPlayerHand';
import ActiveBookArea from './components/ActiveBookArea';
import SpadesGameModel from "./engine/SpadesGameModel";
import SpadesRobot from "./engine/SpadesRobot";

const BID_DELAY = 100; // ideal default value ~1000
const DELAY = 100; // ideal default value ~1000
const TRICK_COMPLETED_DELAY = 1000; // ideal default value ~2000
const PARTNER_AI_LEVEL = 1; // smart
const OPPONENT_AI_LEVEL = 1; // dumb
const MAX_SCORE = 150;

class App extends Component {

  state = {
    game: null,
    gameState: null,
    currentHandState: null,
    selectedCard: null,
    lastTrickWinner: 1,
    autoHand: false,
    showLastTrick: false,
  };

  setSelectedCard = (value) => {
    // if the given card is already selected, then they're trying to play a card
    if (value === this.state.selectedCard) {
      this.log('playing selected card value = ', value);
      const currentHand = this.state.game.getCurrentHand();
      const number = value.length === 2 ? value[0] : (value[0] + value[1]);
      const suit = value.length === 2 ? value[1] : value[2];
      currentHand.playCard(1, {number, suit });
    } else {
      this.setState({selectedCard: value});
    }
  };

  log = (msg) => {
    console.log(msg);
  };

  toggleAutoHand = () => {
    this.setState({ autoHand: !this.state.autoHand }, () => {
      // after the new value is saved into state, trigger a fake state change
      // so the UI will potentially make a move on the users' behalf
      this.handleStateChange();
    });
  };

  toggleShowLastTrick = () => {
    this.setState({ showLastTrick: !this.state.showLastTrick });
  };

  handleTrickCompletedCallback = (trickWinner, board) => {
    let currentHandState = this.state.currentHandState;
    currentHandState.board = board;
    this.setState({currentHandState});
    setTimeout( () => {
      this.setState({
        lastTrickWinner: trickWinner
      });
      this.handleStateChange();
    }, TRICK_COMPLETED_DELAY)
  };

  handleStateChange = () => {
    if (!this.state.game) return null;

    const game = this.state.game;
    let handState = game.getCurrentHand().getHandState();
    this.setState({currentHandState: handState});
    this.setState({gameState: this.state.game.getGameState()});

    // // debugging
    // this.log('state change invoked');
    // this.log(this.state.game.getGameStateString() + '\n');
    // this.log(this.state.game.getCurrentHand().getHandStateString() + '\n\n');
    // // debugging

    // handle robot bidding
    if( handState.states.BIDDING === handState.state ) {
      // if the current bidder is player 2-4 (robots), then defer to the robot for the bid
      if(this.state.autoHand || handState.currentBiddersTurn > 1) {
        var playerHand = handState.playerHands[handState.currentBiddersTurn-1],
          robotBid = SpadesRobot.getBid(playerHand);

        // put an artificial lag on the bidding
        setTimeout(function(){
          game.getCurrentHand().applyPlayerBid( handState.currentBiddersTurn, robotBid );
        }, BID_DELAY);
      }
    } else if (handState.states.PLAYING === handState.state) {
      // if the current player is player 2-4 (robots), then defer to the robot for the bid
      if (this.state.autoHand || handState.currentPlayersTurn > 1) {
      // if (handState.currentPlayersTurn > 0) {
        let cardWasAccepted = false;
        let currentHand = this.state.game.getCurrentHand();
        handState = currentHand.getHandState();
        if (handState.currentPlayersTurn >= 1) {
          // canPlayCard returns true/false to indicate whether the card play was successful
          while (!cardWasAccepted) {
            var aiLevel = (handState.currentPlayersTurn === 3 || handState.currentPlayersTurn === 1) ? PARTNER_AI_LEVEL : OPPONENT_AI_LEVEL;
            var robotCardSelection = SpadesRobot.getCardSelection(handState.currentPlayersTurn, currentHand, aiLevel);
            cardWasAccepted = currentHand.canPlayCard(handState.currentPlayersTurn, robotCardSelection);

          }
          setTimeout( () => {
            currentHand.playCard(handState.currentPlayersTurn, robotCardSelection);
          }, DELAY);
        }
      }
    } else if (handState.states.INITIALIZED === handState.state) {
      game.getCurrentHand().startBidding();
    }

  };

  handleNewGameClick = () => {
    let game = new SpadesGameModel({
      playerNames: ["You", "GriffDawg", "Beth", "Megatron"],
      printDebugInfo: true,
      trickCompletedCallback: this.handleTrickCompletedCallback,
      // gameCompletedCallback: self.handleGameCompletedEvent,
      stateChangeCallback: this.handleStateChange,
      maxScore: MAX_SCORE
    });
    this.setState({game}, () => {
      game.getCurrentHand().startBidding();
    });
  };

  handleBidSelection = (amount) => {
    // 1-based player number (1-4)
    this.state.game.getCurrentHand().applyPlayerBid(1, amount);
  };

  renderCardTable() {
    const handState = this.state.currentHandState;
    const playerNames = this.state.gameState.playerNames;
    const bids = handState.bids || [];
    const bookCounts = handState.bookCounts || [];
    const playerIsBidding = handState.states.BIDDING === handState.state && handState.currentBiddersTurn === 1;

    const playerCards = handState.playerHands[0].map( (c) => {
      const value = c.number + c.suit.abbrev;
      return {
        value,
        selected: this.state.selectedCard === value
      }
    });

    return (
      <div className="cardTable">
        {playerIsBidding && <BidPanel onSelectedBid={this.handleBidSelection}/>}
        <div className="gridRow">
          <div className={`spot partnerSpot ${handState.currentBiddersTurn === 3 || handState.currentPlayersTurn === 3 ? 'playersTurn' : ''}`}>
            <div className="playerName">{playerNames[2]}</div>
            <div className="bookPanel">{bookCounts[2]}/{bids[2]}</div>
            <OtherPlayerHand/>
          </div>
        </div>
        <div className="gridRow gridRowMiddle">
          <div className={`spot opponentSpot leftOpponent ${handState.currentBiddersTurn === 2 || handState.currentPlayersTurn === 2 ? 'playersTurn' : ''}`}>
            <div className="playerName">{playerNames[1]}</div>
            <div className="bookPanel">{bookCounts[1]}/{bids[1]}</div>
            <OtherPlayerHand/>
          </div>
          <ActiveBookArea lastTrickWinner={this.state.lastTrickWinner} board={handState.board}/>
          <div className={`spot opponentSpot rightOpponent ${handState.currentBiddersTurn === 4 || handState.currentPlayersTurn === 4 ? 'playersTurn' : ''}`}>
            <div className="playerName">{playerNames[3]}</div>
            <div className="bookPanel">{bookCounts[3]}/{bids[3]}</div>
            <OtherPlayerHand/>
          </div>
        </div>
        <div className="gridRow">
          <div className={`spot playerSpot ${handState.currentBiddersTurn === 1 || handState.currentPlayersTurn === 1 ? 'playersTurn' : ''}`}>
            <div className="playerName">{playerNames[0]}</div>
            <div className="bookPanel">{bookCounts[0]}/{bids[0]}</div>
            <PlayerCards cards={playerCards} onSelectedCard={this.setSelectedCard}/>
          </div>
        </div>
      </div>
    );
  }

  renderLastTrickBox = () => {
    const handHistory = this.state.currentHandState.handHistory;

    if (handHistory.length > 0) {
      let lastHand = handHistory[handHistory.length - 1];
      let lastHandSorted = [];
      for (let i = 0; i < lastHand.length; ++i) {
        let curCard = lastHand[i];
        lastHandSorted[curCard.playedBy - 1] = curCard;
      }
      return <ActiveBookArea board={lastHandSorted} lastTrickWinner={1} className="lastTrick"/>;
    }

  };

  render() {
    const gameState = this.state.gameState;
    return (
      <div className="App">
        <div className="controlPanel">
          <button onClick={this.handleNewGameClick}>New Game</button>
          <label>
            <input name="autoHand" type="checkbox" checked={this.state.autoHand} onChange={this.toggleAutoHand}/>
            AutoHand
          </label>
          {this.state.game && this.state.currentHandState && <button onClick={this.toggleShowLastTrick}>Last Trick</button>}
          {this.state.gameState && (
            <div className="scorePanel">
              <span className="usScore">US: {gameState.score[0]}</span>
              <span>THEM: {gameState.score[1]}</span>
            </div>
          )}
        </div>
        {this.state.game && this.state.currentHandState && this.renderCardTable()}

        {this.state.showLastTrick && (
          <div className="modalPopup" onClick={this.toggleShowLastTrick}>
            <div className="modalPopupInner">
              {this.renderLastTrickBox()}
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default App;