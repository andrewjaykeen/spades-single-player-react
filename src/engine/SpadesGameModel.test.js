import SpadesGameModel from './SpadesGameModel';
import SpadesRobot from './SpadesRobot';

function noop() {  return {};  }

// TODO: use sinon to have spies and such (for calledOnce, etc)

const MAX_SCORE = 300;
const PLAYER_NAMES = ["You", "Opponent 2", "Your Partner", "Opponent 2"]
const gameOptions = {
  playerNames: PLAYER_NAMES,
  printDebugInfo: false,
  trickCompletedCallback: noop,
  gameCompletedCallback: noop,
  stateChangeCallback: noop,
  maxScore: MAX_SCORE
};



function completeTestHand(hand, printDebugInfo) {
  hand.startBidding();  // transition from INITIALIZED to BIDDING state
  let handState = hand.getHandState();

  for(let playerNum=1; playerNum < 5; ++playerNum) {
    let playerHand = handState.playerHands[playerNum-1],
      robotBid = SpadesRobot.getBid(playerHand);

    hand.applyPlayerBid(playerNum, robotBid);
  }

  handState = hand.getHandState();
  if(printDebugInfo) {  console.log(hand.getHandStateString());  }
  while( handState.state !== "Completed" ) {
    let robotIntelLevel = handState.currentPlayersTurn % 2,  // even number players (team 2) are dumb
      card = SpadesRobot.getCardSelection(handState.currentPlayersTurn, hand, robotIntelLevel);
    hand.playCard(handState.currentPlayersTurn, card);
    handState = hand.getHandState();
    //console.log(hand.getHandStateString());  // still playing...
  }

  // game should be over now!
  if(printDebugInfo) {
    console.log(hand.getHandStateString());
    console.log(hand.getHandHistoryString());
  }
}


function runGameSimulation(printDebugInfo) {
  let game = new SpadesGameModel({
    playerNames: ["Andy", "Joe", "Peter", "Robert"],
    printDebugInfo: printDebugInfo
  });
  let gameState = game.getGameState(),
    loopCount = 0;
  while( gameState.state != "Completed" ) {
    if(loopCount > 100) {
      break;
    }
    completeTestHand(game.getCurrentHand(), printDebugInfo);
    gameState = game.getGameState();
    if(printDebugInfo) {
      console.log(game.getGameStateString());
    }
    ++loopCount;
  }
  if(printDebugInfo) {
    console.log(game.getGameStateString());
  }
  return gameState.winner;
}



const NUM_SIM_GAMES_EXEC = 100;

it(`can complete ${NUM_SIM_GAMES_EXEC} games without failing`, () => {
  let team1WinCount = 0,
    team2WinCount = 0,
    unfinished = 0;
  // process.stdout.write("Running");
  for(let i=0; i < NUM_SIM_GAMES_EXEC; ++i) {
    // -1 = no winner, 1 = team 1 and 2 = team 2
    let winner = runGameSimulation(false);
    if(winner == 1) team1WinCount++;
    if(winner == 2) team2WinCount++;
    if(winner == -1) unfinished++;
    // process.stdout.write(".");
  }
  // console.log('team1WinCount = ', team1WinCount);
  // console.log('team2WinCount = ', team2WinCount);
  // console.log('unfinished games = ', unfinished);

  // we should have a 90% win rate for the "smart" team
  expect(team1WinCount).toBeGreaterThan(NUM_SIM_GAMES_EXEC * 0.9);

  // should definitely have less than 5% of the games be unfinished
  expect(unfinished).toBeLessThan(NUM_SIM_GAMES_EXEC * 0.05);
});


it('new game honors options', () => {
  const game = new SpadesGameModel(gameOptions);
  const gameState = game.getGameState();

  expect(gameState.state).toEqual('Playing');
  expect(gameState.maxScore).toEqual(MAX_SCORE);
  expect(gameState.playerNames).toEqual(PLAYER_NAMES);
  expect(gameState.score).toEqual([0,0]);
  expect(gameState.handHistory).toEqual([]);
  expect(gameState.winner).toEqual(-1);
});

it('creates a new SpadesHandModel instance', () => {
  const game = new SpadesGameModel(gameOptions);
  const handState = game.getCurrentHand().getHandState();

  // console.log(game.getCurrentHand().getHandState());
  expect(handState.state).toEqual('Initialized');
  expect(handState.spadesHaveBeenBroken).toEqual(false);
  expect(handState.currentBiddersTurn).toEqual(1);
  expect(handState.bids).toEqual([ -1, -1, -1, -1 ]);
});
