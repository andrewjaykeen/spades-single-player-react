

class SpadesHandModel {
  constructor(config) {
    this.playerNames = config.playerNames || ["Player 1", "Player2", "Player 3", "Player 4"];
    this.handCompletedCallback = config.handCompletedCallback || null;
    this.trickCompletedCallback = config.trickCompletedCallback || null;
    this.stateChangeCallback = config.stateChangeCallback || null;
    this.bidAppliedCallback = config.bidAppliedCallback || null;
    this.cardPlayedCallback = config.cardPlayedCallback || null;
    this.printDebugInfo = config.printDebugInfo || false;

    this.cardNumberList = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
    this.suitOrder = ["H", "C", "D", "S"];  // the order that the suits appear in the player's hand
    this.suits = [
      {name: "Hearts", abbrev: "H"},
      {name: "Diamonds", abbrev: "D"},
      {name: "Clubs", abbrev: "C"},
      {name: "Spades", abbrev: "S"}
    ];

    // state variables
    this.handStates = {
      NEW: "New",
      INITIALIZED: "Initialized",
      BIDDING: "Bidding",
      PLAYING: "Playing",
      COMPLETED: "Completed"
    };
    this.handState = this.handStates.NEW;
    this.deck = [];  // array of cards
    this.playerHands = [];  // array of arrays
    this.bids = [-1, -1, -1, -1]; // array of numbers (0-13)
    this.currentBiddersTurn = 1;  // possible values: 1-4
    this.currentPlayersTurn = -1;  // possible values: 1-4
    this.spadesHaveBeenBroken = false;

    this.board = []; // will have 0-4 cards on it at any given point
    this.handHistory = [];  // array that contains 0-13 rows (boards/rounds)
    this.bookCounts = [0, 0, 0, 0];  // 4-item array containing the number of books a person has received

    this.init();
  }

  init = () => {
    this.log("initializing...");
    this.log(this.playerNames);
    this.createDeck();
    this.shuffleDeck();
    this.dealCards();
    this.sortPlayerHands();
    this.setState(this.handStates.INITIALIZED);
  };

  getHandHistoryString = () => {
    const self = this;
    let buf = "";

    buf += "========== HAND HISTORY ============\n";
    for (let i = 0; i < self.handHistory.length; ++i) {
      let roundNumber = i + 1, // 1-13
        curRound = self.handHistory[i];
      if (roundNumber < 10) {
        buf += " ";
      }
      buf += roundNumber + "::   ";
      for (let j = 0; j < curRound.length; ++j) {
        let curCard = curRound[j];
        if (curCard.wonRound) {
          buf += "*";
        }
        else {
          buf += " ";
        }
        if (curCard.number.length === 1) {
          buf += " ";
        }
        buf += curCard.number + curCard.suit.abbrev + " (p" + curCard.playedBy + ")";
        buf += "      ";
      }
      buf += "\n";
    }
    buf += "========== HAND HISTORY ============\n";
    return buf;
  };

  getHandStateString = () => {
    const self = this;
    let theHandState = self.getHandState(),
      buf = "";
    buf += "=========== HAND STATE =========\n";
    buf += "hand state = " + theHandState.state + "\n";
    buf += "currentBiddersTurn = " + theHandState.currentBiddersTurn + "\n";
    buf += "currentPlayersTurn = " + theHandState.currentPlayersTurn + "\n";
    buf += "spadesHaveBeenBroken = " + theHandState.spadesHaveBeenBroken + "\n";
    for (let i = 0; i < theHandState.playerHands.length; ++i) {
      let playerHand = theHandState.playerHands[i],
        playerName = theHandState.playerNames[i],
        bid = theHandState.bids[i],
        booksMade = theHandState.bookCounts[i],
        playerNumber = i + 1,
        handString = `Player #${playerNumber} (${playerName}) :: BID ${bid} :: BOOKS ${booksMade} ::  `;
      for (let j = 0; j < playerHand.length; ++j) {
        let card = playerHand[j];
        handString += card.number + card.suit.abbrev + "  ";
      }
      buf += handString + "\n";
    }

    // board
    buf += "BOARD STATE: ";
    for (let i = 0; i < theHandState.board.length; ++i) {
      let card = theHandState.board[i];
      buf += card.number + card.suit.abbrev + "   ";
    }
    buf += "\n";

    // TODO: handHistory

    buf += "=========== HAND STATE =========\n";

    return buf;
  };


  /*
   *  canPlayCard
   *
   * @param playerNumber: 1-based (1-4)
   * @param card: object:  { number: "3", suit: "S" }
   * @return true on success, false for invalid state or params
   */
  canPlayCard = (playerNumber, card) => {
    const self = this;

    if (self.handState !== self.handStates.PLAYING) {
      self.log("ERROR: playCard() invoked with an invalid hand state: " + self.handState);
      return false;
    }

    if (playerNumber !== self.currentPlayersTurn) {
      self.log("ERROR: playCard() invoked with a playerNumber that doesn't match self.currentPlayersTurn: " + playerNumber + " and: " + self.currentPlayersTurn);
      return false;
    }

    // ensure the user has the card they're attempting to play
    let playerHand = self.playerHands[playerNumber - 1],
      foundCardInPlayersHand = false;

    for (let i = 0; i < playerHand.length; ++i) {
      let curCard = playerHand[i];
      if (curCard.number === card.number && curCard.suit.abbrev === card.suit) foundCardInPlayersHand = true;
    }

    if (!foundCardInPlayersHand) {
      self.log("ERROR: playCard() invoked with a card that the player does not have in their hand: " + self.currentPlayersTurn);
      return false;
    }

    // validation - they can only play the suit that was led with unless they're out of cards
    if (self.board.length > 0) {
      // the player is not attempting to play the first card of the round
      let firstCardPlayedThisRound = self.board[0],
        startingSuit = firstCardPlayedThisRound.suit.abbrev

      if (startingSuit !== card.suit) {
        // the card they're attempting to play does not match the suit of that card that led the round,
        // so we need to ensure that they're entirely out of the starting suit

        if (self.playerHasCardFromSuit(playerHand, startingSuit)) {
          self.log("ERROR: playCard() invoked with a card does not match the starting suit (" + startingSuit + ") and the user has a card in that suit.  Card played =" + card.number + card.suit);
          return false;
        }

      }
    }
    else {
      // the player is playing the first card on the board

      // ensure that spades has been broke or they only have spades left
      if (card.suit === "S" && !self.playerHasOnlySpades(playerHand)) {
        if (!self.spadesHaveBeenBroken) {
          self.log("ERROR: playCard() inoked - player attempting to lead with spades even though it hasn't been broken and they're not out of spades.  Card played =" + card.number + card.suit);
          return false;
        }
      }
    }

    return true;
  };


  /*
   *  playCard
   *
   * @param playerNumber: 1-based (1-4)
   * @param card: object:  { number: "3", suit: "S" }
   * @return true on success, false for invalid state or params
   */
  playCard = (playerNumber, card) => {
    const self = this;

    if (!self.canPlayCard(playerNumber, card)) return false;

    // validation above was successful!
    // now remove the card from the players hand and put it on the board.
    // note: we must loop backwards since we're removing from the array as we iterate.
    let playerHand = self.playerHands[playerNumber - 1]
    let len = playerHand.length;
    while (len--) {
      let curCard = playerHand[len];
      if (curCard.number === card.number && curCard.suit.abbrev === card.suit) {
        playerHand.splice(len, 1);  // remove
        curCard.playedBy = playerNumber;  // which player played the card
        self.board.push(curCard);
      }
    }

    if (card.suit === "S") {
      self.spadesHaveBeenBroken = true;
    }

    // adjust/increment self.currentPlayersTurn
    if (playerNumber === 4) {
      self.currentPlayersTurn = 1;
    } else {
      self.currentPlayersTurn++;
    }

    // if we made it this far then we have put a card on the board.
    // and we should check to see if the board size (array count) has reached 4,
    // if so, then we push self.board onto the hand history and then clear the board (self.board = []),
    // then proceed to determine who "won" the board/round and adjust self.currentPlayersTurn accordingly.

    if (self.board.length === 4) {

      // determine who won the board/round
      let trickWinner = self.determineRoundWinner(self.board);

      // update the board
      for (let i = 0; i < self.board.length; ++i) {
        let curCard = self.board[i];
        if (curCard.playedBy === trickWinner) curCard.wonRound = true;
      }

      if (self.trickCompletedCallback !== null) {
        self.trickCompletedCallback(trickWinner, self.board);
      }

      self.handHistory.push(self.board);
      self.board = [];  // empty

      // adjust currentPlayersTurn accordingly
      self.currentPlayersTurn = trickWinner;

      // increment the book count for the winning player
      self.bookCounts[trickWinner - 1]++;  // 0-based index on bookCounts array
    } else {
      // fire the card played event
      if (self.cardPlayedCallback) {
        self.cardPlayedCallback();
      }
    }

    // check if the game is over...
    if (self.handHistory.length === 13) {
      // game is over!
      self.setState(self.handStates.COMPLETED);
      self.currentPlayersTurn = -1;

      if (self.handCompletedCallback !== null) {
        self.handCompletedCallback();
      }

      // TODO: reset other state variables that are not applicable now that the game is over?
    }

    return true;  // success! card was played!
  }


  /*
   *  determineRoundWinner
   *
   *  @param board - array of 4 cards
   *  @return playerNumber (1-4) that won the hand
   */
  determineRoundWinner = (board) => {
    // todo: loop through all the card and figure out the highest
    // use playedBy and card.number and card.suit.abbrev to compare
    // NOTE: the lead card is always board[0] since its sorted.

    let highestCard = board[0];
    for(let i=1; i < 4; ++i) {
      let curCard = board[i];
      if(curCard.suit.abbrev === highestCard.suit.abbrev) {
        if (this.cardNumberList.indexOf(curCard.number) > this.cardNumberList.indexOf(highestCard.number)) {
          highestCard = curCard;
        }
      } else if( curCard.suit.abbrev === "S" && highestCard.suit.abbrev !== "S") {
        highestCard = curCard;
      }
    }
    return highestCard.playedBy;  // who played the card
  };


  playerHasCardFromSuit = (playerHand,suitToCheckFor) => {
    let hasCardFromSuit = false;
    for(let i=0; i < playerHand.length; ++i) {
      let curCard = playerHand[i];
      if(curCard.suit.abbrev === suitToCheckFor) hasCardFromSuit = true;
    }
    return hasCardFromSuit;
  };

  playerHasOnlySpades = (playerHand) => {
    let userHasOnlySpades = true;
    for(let i=0; i < playerHand.length; ++i) {
      let curCard = playerHand[i];
      if(curCard.suit.abbrev !== "S") userHasOnlySpades = false;
    }
    return userHasOnlySpades;
  };

  startBidding = () => {
    if( this.handStates.INITIALIZED === this.handState ) {
      this.setState(this.handStates.BIDDING);
    }
  };



  /*
   *  applyPlayerBid
   *
   * @param playerNumber: 1-based (1-4)
   * @param bid: number: 0-13
   *
   * @return true on success, false for invalid hand state or params
   */
  applyPlayerBid = (playerNumber, bid) => {
    const self = this;
    
    if( self.handStates.BIDDING !== self.handState ) {
      self.log("ERROR: applyPlayerBid invoked with an invalid hand state: " + self.handState);
      return false;
    }

    if(playerNumber !== self.currentBiddersTurn) {
      self.log("ERROR: applyPlayerBid invoked with a playerNumber that doesn't match self.currentBiddersTurn: " + playerNumber + " and: " + self.currentBiddersTurn);
      return false;
    }

    if( !( bid > -1 && bid < 14) ) {
      self.log("ERROR: applyPlayerBid invoked with bad bid value: " + bid);
      return false;
    }

    // apply the bid for the player
    self.bids[playerNumber-1] = bid;

    // if it was the last bid, change the hand state to PLAYING.  otherwise change it to BIDDING
    if(playerNumber === 4) {
      // last bid
      self.setState(self.handStates.PLAYING);
      self.currentBiddersTurn = -1;  // indicating bidding is done
      self.currentPlayersTurn = 1;  // initializing
    } else {
      // still bidding
      self.currentBiddersTurn = self.currentBiddersTurn+1;  // increment
    }

    if(self.bidAppliedCallback){ self.bidAppliedCallback(); }

    return true;
  };


  // sort the player hands (self.playerHands) in order of cardNumber and suit
  sortPlayerHands = () => {
    const self = this;
    
    for(let i = 0; i < 4 ; ++i) {
      let playerHand = self.playerHands[i];

      playerHand.sort(function(a, b) {

        // first sort by suit
        if (self.suitOrder.indexOf(a.suit.abbrev) < self.suitOrder.indexOf(b.suit.abbrev))
          return -1;
        if (self.suitOrder.indexOf(a.suit.abbrev) > self.suitOrder.indexOf(b.suit.abbrev))
          return 1;

        // if the suits are even, then sort by card number
        if (self.cardNumberList.indexOf(a.number) < self.cardNumberList.indexOf(b.number))
          return -1;
        if (self.cardNumberList.indexOf(a.number) > self.cardNumberList.indexOf(b.number))
          return 1;

        // invalid case - no two people should ever have the same exact card in thier hand
        return 0;
      });

    }
  };

  dealCards = () => {
    this.playerHands[0] = this.deck.slice(0,13);
    this.playerHands[1] = this.deck.slice(13,26);
    this.playerHands[2] = this.deck.slice(26,39);
    this.playerHands[3] = this.deck.slice(39,52);
    this.deck.length = 0;  // empty the deck
  };

  shuffleDeck = () => {
    let currentIndex = this.deck.length, temporaryValue, randomIndex;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = this.deck[currentIndex];
      this.deck[currentIndex] = this.deck[randomIndex];
      this.deck[randomIndex] = temporaryValue;
    }
  };

  createDeck = () => {
    for(let i=0; i < this.suits.length; ++i) {
      let curSuit = this.suits[i];
      for( let j=0; j < this.cardNumberList.length; ++j) {
        let curCardNumber = this.cardNumberList[j];
        let currentCard = { number: curCardNumber, suit: curSuit };
        this.deck.push(currentCard);
      }
    }
  };

  getHandState = () => {
    return {
      state: this.handState,
      states: this.handStates,
      playerHands: this.playerHands,
      playerNames: this.playerNames,
      bids: this.bids,
      currentBiddersTurn: this.currentBiddersTurn,
      currentPlayersTurn: this.currentPlayersTurn,
      board: this.board,
      handHistory: this.handHistory,
      spadesHaveBeenBroken: this.spadesHaveBeenBroken,
      bookCounts: this.bookCounts
    };
  };

  setState = (newState) => {
    this.handState = newState;
    if(this.stateChangeCallback) {  this.stateChangeCallback(); }
  };

  log = (msg) => {
    if(this.printDebugInfo) console.log(msg);
  };
  
}

export default SpadesHandModel;


/*
  NOTE: these are the only "public" methods/properties for this class
 return {
    getHandStateString: self.getHandStateString,
    applyPlayerBid: self.applyPlayerBid,
    playCard: self.playCard,
    canPlayCard: self.canPlayCard,
    getHandState: self.getHandState,
    cardNumberList: self.cardNumberList,
    suitOrder: self.suitOrder,
    getHandHistoryString: self.getHandHistoryString,
    startBidding: self.startBidding
  };
 */
