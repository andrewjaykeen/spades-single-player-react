
// TODO: eventually pass the game model object into these static functions so they have more info to pull from

class SpadesRobot {

  static getBid = (playerHand) => {
    let bid = 0, totalLowerSpades = 0;
    for(let i=0; i < playerHand.length; ++i) {
      let card = playerHand[i];

      // adjust robot intelligence here: add/remove 'Q' or 'J' to the array to affect bid
      if( ['A','K','Q'].indexOf(card.number) > -1 ) {
        ++bid;
      } else if( card.suit.abbrev === 'S') {
        ++totalLowerSpades;
      }
    }
    // adjust robot intelligence here: increment/decrement the affect on the bid here
    if(totalLowerSpades > 3){ bid += totalLowerSpades - 3; }
    if(bid < 1) bid = 1;  // not handling nil for now
    return bid;
  };


  /*
   * @param playerNumber (1-4)
   * @param spadesHandModel
   * @param intelligenceLevel 0-n with zero being the lowest
   */
  static getCardSelection = (playerNumber, spadesHandModel, intelligenceLevel) => {
    let handState = spadesHandModel.getHandState(),
      playerHand = handState.playerHands[playerNumber-1],
      playerBid = handState.bids[playerNumber-1],
      playerTrickCount = handState.bookCounts[playerNumber-1],
      partnerNumbers = [3,4,1,2],  // player 1's partner is player 3, etc...
      partnerNumber = partnerNumbers[playerNumber-1],
      partnerBid = handState.bids[partnerNumber-1],
      partnerTrickCount = handState.bookCounts[partnerNumber-1],
      totalBid = playerBid + partnerBid,
      totalTrickCount = playerTrickCount + partnerTrickCount,
      teamHasTheirBooks = (totalBid < totalTrickCount),
      playerHasTheirBooks = (playerBid < playerTrickCount),
      // if we don't have all our own books or our team doesn't have their books and its one of the last few tricks in the hand (help our partner out)
      tryToWinTrick = (!teamHasTheirBooks && (!playerHasTheirBooks || playerHand.length < 4)),
      cardToPlay = null;

    // dumb robots just play random cards
    if(intelligenceLevel < 1) {
      let cardAccepted = false;
      while(!cardAccepted) {
        let randomCard = playerHand[  Math.floor((Math.random() * playerHand.length))  ];
        cardToPlay = {number: randomCard.number, suit: randomCard.suit.abbrev }
        cardAccepted = spadesHandModel.canPlayCard(playerNumber, cardToPlay);
      }
      return cardToPlay;
    }


    if( tryToWinTrick ) {
      // play the highest card we have in the leading suit (if applicable - or cut with spades if we have it)

      if( handState.board.length > 0 ) {
        // there's something already on the board, see what the leading card is.
        let leadingCard = handState.board[0];
        //console.log("leading card number = " + leadingCard.number + " and suit = " + leadingCard.suit.abbrev);

        let sortedPlayerHand = SpadesRobot.sortPlayerHandBySuit(playerHand);
        if(sortedPlayerHand[leadingCard.suit.abbrev].length > 0) {
          //console.log("WE HAVE A CARD IN THAT SUIT!");
          // pick the highest card in that suit
          let cardNumberToPlay = sortedPlayerHand[leadingCard.suit.abbrev][ sortedPlayerHand[leadingCard.suit.abbrev].length-1 ];
          cardToPlay = {number: cardNumberToPlay, suit: leadingCard.suit.abbrev};

          // TODO: make sure we're not wasting it by seeing if the board already has a higher card!!!
          // basically simple if(boardHasHigherCard than cardToPlay),
          // then pick the first item in sortedPlayerHand array (lowest card)

        } else if(sortedPlayerHand['S'].length > 0) {
          //console.log("WE DO NOT HAVE A CARD IN THE LEADING SUIT BUT WE DO HAVE A SPADE!!!");
          // pick the lowest spade

          // TODO: loop all through all the spades in the users hand (lowest to highest)
          // until we find the lowest spade in our hand that is higher than a spade on the board.
          // if we don't have any spades higher than one on the board (that has already cut), then
          // we want to throw off.  maybe we should make a "throw off" function that can be called from
          // multiple spots?  maybe a simple implementation at first that just plays the lowest card from
          // a non-spades suit unless we only have spades, in which case we play the lowest one of those?
          // hmm... the array let 'playerHand' here is already sorted left-to-right lowest-to-highest with spades
          // on the right, that should be all we need.

          let cardNumberToPlay = sortedPlayerHand['S'][0];
          cardToPlay = {number: cardNumberToPlay, suit: 'S' };
        } else {
          //console.log("WE DO NOT HAVE A CARD IN THAT SUIT AND WE HAVE NO SPADES!");
          // TODO: pick a random LOW card from the hand (throw off)
          // (implicitly by falling through to the bottom where we handle cardToPlay==null)
        }
      } else {
        // we're playing the first card

        // play the highest card in any suit we can (we check if spades broke yet)
        cardToPlay = SpadesRobot.getHighestNumberedCardInHand(playerHand, handState.spadesHaveBeenBroken );
      }

    } else {
      // TODO:
      // if we DONT want the book (determined in step 1), then we want to throw off, meaning we want to throw
      // the highest non-spades suits (unless the leading card was non spades AND its been cut by someone else)
      // this whole else block is basically throwing off
    }


    if( cardToPlay == null || !spadesHandModel.canPlayCard(playerNumber, cardToPlay) ) {
      //console.log("PLAYING RANDOM CARD!!!  this should never happen because we're suppose to find the perfect card every time!");
      // play a random card from the players hand - it's okay if it fails, we'll keep looping till we find one that works
      let cardAccepted = false;
      while(!cardAccepted) {
        let randomCard = playerHand[  Math.floor((Math.random() * playerHand.length))  ];
        cardToPlay = {number: randomCard.number, suit: randomCard.suit.abbrev }
        cardAccepted = spadesHandModel.canPlayCard(playerNumber, cardToPlay);
      }
    } else {
      //console.log("PLAYED A CHOSEN CARD!");
    }

    return cardToPlay;
  };


  // this is across all suits (spades dependent on flag param)
  static getHighestNumberedCardInHand = (playerHand, includeSpades) => {
    let numberOrder = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"],
      highestCard = null;
    for(let i= 0; i < playerHand.length; ++i) {
      let curCard = playerHand[i];
      if(highestCard == null) {
        highestCard = curCard;
      } else if (numberOrder.indexOf(curCard.number) > numberOrder.indexOf(highestCard.number)) {
        // console.log(curCard.suit.abbrev);
        if(curCard.suit.abbrev !== 'S' || (curCard.suit.abbrev === 'S' && includeSpades) ) {
          highestCard = curCard;
        }
      }
    }
  };


  /*
   *  sort the player hand based on suit.
   * return an object with up to 4 keys: S, H, C, D and the value is an array of cards (pre-sorted)
   */
  static sortPlayerHandBySuit = (playerHand) => {
    let sortedHand = { 'S': [], 'H': [], 'C': [], 'D': [] };
    for(let i= 0; i < playerHand.length; ++i) {
      let card = playerHand[i];
      sortedHand[card.suit.abbrev].push(card.number);
    }
    return sortedHand;
  };
}



export default SpadesRobot;