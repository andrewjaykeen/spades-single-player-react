import SpadesHandModel from "./SpadesHandModel";


class SpadesGameModel {
  constructor(config) {
    this.playerNames = config.playerNames || ["Player 1", "Player 2", "Player 3", "Player 4"];
    this.maxScore = config.maxScore || 500;
    this.stateChangeCallback = config.stateChangeCallback || null;
    this.printDebugInfo = config.printDebugInfo || null;
    this.trickCompletedCallback = config.trickCompletedCallback || null;
    this.gameCompletedCallback = config.gameCompletedCallback || null;
    this.gameNumber = Math.floor(Math.random() * 555555);
    this.gameStates = { NEW: "New", PLAYING: "Playing", COMPLETED: "Completed" };
    this.gameState = this.gameStates.NEW;
    this.playerIndicesByTeam = [[0,2], [1,3]];  // players 1 & 3 = team 1, players 2 & 4 = team 2
    this.currentHand = null;
    this.score = [0,0];  // players 1 & 3 = team 1, players 2 & 4 = team 2
    this.handHistory = [];  // array of SpadesHandModel objects (oldest to newest)
    this.winner = -1;  // possible values team 1 (players 1&3) or team 2 (players 2&4)

    this.init();
  }

  // every time a SpadesHandModel is complete, they let us know about it
  handCompletedCallback = () => {
    const self = this;

    // update the score with the results from self.currentHand (which just completed)
    let handState = self.currentHand.getHandState();

    // teamIndexNumber is zero-based here(0,1) = (team 1, team 2)
    for(let teamIndexNumber=0; teamIndexNumber < 2; ++teamIndexNumber) {
      let teamPlayer1 = self.playerIndicesByTeam[teamIndexNumber][0],
        teamPlayer2 = self.playerIndicesByTeam[teamIndexNumber][1];

      let teamTotalBid = handState.bids[teamPlayer1] + handState.bids[teamPlayer2];
      let teamTricksMade = handState.bookCounts[teamPlayer1] + handState.bookCounts[teamPlayer2];

      // check if they got set on the nil...
      for(let i = 0; i < self.playerIndicesByTeam[teamIndexNumber].length ; ++i ) {
        let currentTeamPlayerIndex = self.playerIndicesByTeam[teamIndexNumber][i],
          bid = handState.bids[currentTeamPlayerIndex],
          tricksMade = handState.bookCounts[currentTeamPlayerIndex];

        if(bid === 0 && tricksMade > 0) {
          self.log("TEAM " + (teamIndexNumber+1) + " busted their nil!");
          self.score[teamIndexNumber] -= 100;  // subtract 100 points
        }

        if(bid === 0 && tricksMade === 0) {
          self.log("TEAM " + (teamIndexNumber+1) + " made their nil!");
          self.score[teamIndexNumber] += 100;  // add 100 points
        }

      }

      self.log("TEAM " + (teamIndexNumber+1) + " teamTotalBid=" + teamTotalBid + ", teamTricksMade=" +teamTricksMade);
      let teamTotalPointsForCurrentHand = teamTotalBid * 10;
      if( teamTricksMade < teamTotalBid ) {
        // they got set!
        self.log("TEAM " + (teamIndexNumber+1) + " got set!");
        self.log("TEAM " + (teamIndexNumber+1) + " old score: " + self.score[teamIndexNumber]);
        self.score[teamIndexNumber] += (teamTotalPointsForCurrentHand * -1);
        self.log("TEAM " + (teamIndexNumber+1) + " new score: " + self.score[teamIndexNumber]);
      } else {
        // they made their books
        self.log("TEAM " + (teamIndexNumber+1) + " made their books!");
        self.log("TEAM " + (teamIndexNumber+1) + " old score: " + self.score[teamIndexNumber]);
        self.score[teamIndexNumber] += teamTotalPointsForCurrentHand;
        self.log("TEAM " + (teamIndexNumber+1) + " new score: " + self.score[teamIndexNumber]);

        if( teamTricksMade > teamTotalBid ) {
          // they accumulated some bags
          let newBagCount = teamTricksMade - teamTotalBid;
          self.log("TEAM " + (teamIndexNumber+1) + " bagged! new bags they got: " + newBagCount);

          let currentGameScore = self.score[teamIndexNumber];
          let numExistingBagsForTeam = parseInt(currentGameScore.toString().charAt(currentGameScore.toString().length-1),10);  // the last digit
          self.log("TEAM " + (teamIndexNumber+1) + " numExistingBagsForTeam: " + numExistingBagsForTeam);
          if( (numExistingBagsForTeam + newBagCount) >= 10 ) {
            // take them back 100 points;
            self.log("TEAM " + (teamIndexNumber+1) + " deducting 100 points from score.  old score: " + self.score[teamIndexNumber]);
            self.score[teamIndexNumber] = self.score[teamIndexNumber] - 100;
            self.score[teamIndexNumber] -= numExistingBagsForTeam;  // back down to where last digit is zero
            self.score[teamIndexNumber] += ((numExistingBagsForTeam + newBagCount)-10); // add the remainder
            self.log("TEAM " + (teamIndexNumber+1) + " deducting 100 points from score.  new score: " + self.score[teamIndexNumber]);
          } else {
            // add the bag count to their score
            self.score[teamIndexNumber] += newBagCount;
          }
        }

      }

      // lowest score they can have is -200
      if(self.score[teamIndexNumber] < -200) {
        self.score[teamIndexNumber] = -200;
      }

    }

    // determine if game is over, ensuring there is not a tie
    if( (self.score[0] >= self.maxScore || self.score[1] >= self.maxScore) && self.score[0] !== self.score[1] ) {
      // game is over. declare a winner and mark game as compelte
      self.winner = self.score[0] > self.score[1] ? 1 : 2;  // team 1 or team 2
      self.setState(self.gameStates.COMPLETED);
      if( self.gameCompletedCallback !== null ) {
        self.gameCompletedCallback(self.winner);
      }
    } else {
      // game is still going.
      self.handHistory.push(self.currentHand);
      self.currentHand = self.createNewHand();
    }
  };

  createNewHand = () => {
    return new SpadesHandModel({
      playerNames: this.playerNames,
      handCompletedCallback: this.handCompletedCallback,
      stateChangeCallback: this.stateChangeCallback,
      bidAppliedCallback: this.stateChangeCallback,
      cardPlayedCallback: this.stateChangeCallback,
      trickCompletedCallback: this.trickCompletedCallback
    });
  };

  getGameStateString = () => {
    const theGameState = this.getGameState();
    return `
      =========== GAME STATE =========
      game number = ${theGameState.gameNumber}
      game state = ${theGameState.state}
      winner = ${theGameState.winner}
      max score = ${theGameState.maxScore}
      score = Team 1 (players 1 & 3): ${theGameState.score[0]}
      score = Team 2 (players 2 & 4): ${theGameState.score[1]}
      number of hands completed = ${theGameState.handHistory.length}
      =========== GAME STATE =========
    `;
  };

  setState = (newState) => {
    const self = this;
    self.gameState = newState;
    if(self.stateChangeCallback) {  self.stateChangeCallback(); }
  };

  getGameState = () => {
    return {
      state: this.gameState,
      playerNames: this.playerNames,
      maxScore: this.maxScore,
      score: this.score,
      handHistory: this.handHistory,
      winner: this.winner,
      gameNumber: this.gameNumber
    };
  };

  getCurrentHand = () => {
    return this.currentHand;
  };

  init = () => {
    this.log("initializing game...");
    this.currentHand = this.createNewHand();
    this.setState(this.gameStates.PLAYING);
  };

  log = (msg) => {
    if(this.printDebugInfo) console.log("Engine: " + msg);
  }

}

export default SpadesGameModel;

// NOTE: these should be the only "public" methods of this class: getGameStateString, getGameState, getCurrentHand
// This should be added to the docs