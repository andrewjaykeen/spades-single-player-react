import React from 'react';
import OtherPlayerHand from './OtherPlayerHand';
import {mount} from "enzyme/build/index";

it('renders some cards for the other player', () => {
  const component = mount(<OtherPlayerHand/>);
  expect(component.find('.card.faceDown').length).toBe(5);
  expect(component.find('.otherPlayerHand').length).toBe(1);
});

