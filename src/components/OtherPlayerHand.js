import React, { Component } from 'react';

class OtherPlayerHand extends Component {
  render() {
    return (
      <div className="otherPlayerHand">
        <div className="card faceDown"/>
        <div className="card faceDown"/>
        <div className="card faceDown"/>
        <div className="card faceDown"/>
        <div className="card faceDown"/>
      </div>
    );
  }
}

export default OtherPlayerHand;
