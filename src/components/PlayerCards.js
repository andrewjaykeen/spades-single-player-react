import React, { Component } from 'react';
import PropTypes from 'prop-types';

class PlayerCards extends Component {
  render() {
    const self = this;
    return (
      <div className="playerCards">
        { this.props.cards &&
        this.props.cards.map(function(card) {
          return (
            <div className={`card-${card.value} card ${card.selected ? 'selected' : ''}`}
                 onClick={ () => { self.props.onSelectedCard(card.value) } }
                 key={card.value}/>
          );
        })
        }
      </div>
    );
  }
}

PlayerCards.propTypes = {
  onSelectedCard: PropTypes.func.isRequired,
  cards: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string.isRequired,
    selected: PropTypes.bool,
  })).isRequired
};

export default PlayerCards;
