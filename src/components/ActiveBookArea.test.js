import React from 'react';
import ActiveBookArea from './ActiveBookArea';

import { mount } from 'enzyme';

it('renders the books in correct positions', () => {
  const board = [{
    number: '5',
    suit: {
      abbrev: 'D'
    }
  }, {
    number: '4',
    suit: {
      abbrev: 'C'
    }
  }, {
    number: 'J',
    suit: {
      abbrev: 'C'
    }
  }, {
    number: 'A',
    suit: {
      abbrev: 'D'
    }
  }];
  const component = mount(<ActiveBookArea board={board} lastTrickWinner={1}/>);
  expect(component.find('.activeBookArea .cardHolder.containsCard').length).toBe(board.length);

  expect(component.find('.activeBookArea .cardHolder.north .card-JC').length).toBe(1);
  expect(component.find('.activeBookArea .cardHolder.east .card-AD').length).toBe(1);
  expect(component.find('.activeBookArea .cardHolder.south .card-5D').length).toBe(1);
  expect(component.find('.activeBookArea .cardHolder.west .card-4C').length).toBe(1);
});
