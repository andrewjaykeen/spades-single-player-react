import React, { Component } from 'react';
import '../scss/BidPanel.css';
import PropTypes from "prop-types";

class BidPanel extends Component {
  render() {
    return (
      <div className="bidPanel unselectable">
        <button onClick={this.props.onSelectedBid.bind(this,1)}>1</button>
        <button onClick={this.props.onSelectedBid.bind(this,2)}>2</button>
        <button onClick={this.props.onSelectedBid.bind(this,3)}>3</button>
        <button onClick={this.props.onSelectedBid.bind(this,4)}>4</button>
        <button onClick={this.props.onSelectedBid.bind(this,5)}>5</button>
        <br/>
        <button onClick={this.props.onSelectedBid.bind(this,6)}>6</button>
        <button onClick={this.props.onSelectedBid.bind(this,7)}>7</button>
        <button onClick={this.props.onSelectedBid.bind(this,8)}>8</button>
        <button onClick={this.props.onSelectedBid.bind(this,9)}>9</button>
        <button onClick={this.props.onSelectedBid.bind(this,10)}>10</button>
        <br/>
        <button onClick={this.props.onSelectedBid.bind(this,11)}>11</button>
        <button onClick={this.props.onSelectedBid.bind(this,12)}>12</button>
        <button onClick={this.props.onSelectedBid.bind(this,13)}>13</button>
        <button className='nil' onClick={ this.props.onSelectedBid.bind(this,0) }>NIL</button>
      </div>
    );
  }
}

BidPanel.propTypes = {
  onSelectedBid: PropTypes.func.isRequired
};


export default BidPanel;
