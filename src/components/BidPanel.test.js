import React from 'react';
import ReactDOM from 'react-dom';
import BidPanel from './BidPanel';

import { shallow, mount, render } from 'enzyme';
import { spy } from 'sinon';

function noop() {  return {};  }

it('renders without crashing', () => {
  const div = document.createElement('div');
  const onSelectedBidSpy = noop;
  ReactDOM.render(<BidPanel onSelectedBid={onSelectedBidSpy}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders all buttons', () => {
  const onSelectedBidSpy = spy();
  const component = mount(<BidPanel onSelectedBid={onSelectedBidSpy}/>);
  expect(component.find('button').length).toBe(14);
});

it('invokes onSelectedBid callback', () => {
  const onSelectedBidSpy = spy();
  const component = mount(<BidPanel onSelectedBid={onSelectedBidSpy}/>);
  component.find('button').first().simulate('click');
  expect(onSelectedBidSpy.calledOnce).toBe(true);
  expect(onSelectedBidSpy.calledWith(1)).toBe(true);
});

// it('requires onSelectedBid callback prop', () => {
//   const onSelectedBidSpy = spy();
//   try {
//     const component = mount(<BidPanel/>);
//
//     // should never reach here
//     expect(1).toBe(2);
//   } catch (err) {
//     expect(1).toBe(1);
//   }
// });
