import React, { Component } from 'react';
import PlayerCards from "./PlayerCards";
import PropTypes from "prop-types";

class ActiveBookArea extends Component {
  render() {
    const board = this.props.board;
    const lastTrickWinner = this.props.lastTrickWinner;
    let northCard, eastCard, southCard, westCard;
    let pos = lastTrickWinner;

    if (board.length > 0) {
      for(var i=0; i < board.length; ++i) {
        var card = board[i];
        if (pos === 1) southCard = card.number + card.suit.abbrev;
        else if (pos === 2) westCard = card.number + card.suit.abbrev;
        else if (pos === 3) northCard = card.number + card.suit.abbrev;
        else if (pos === 4) eastCard = card.number + card.suit.abbrev;
        pos = pos < 4 ? pos + 1 : 1;
      }
    }

    return (
      <div className={`activeBookArea ${this.props.className}`}>
        <div className={`cardHolder north ${northCard ? 'containsCard' : ''}`}>
          {northCard && <div className={`card-${northCard} card`}/>}
        </div>
        <div className={`cardHolder east ${eastCard ? 'containsCard' : ''}`}>
          {eastCard && <div className={`card-${eastCard} card`}/>}
        </div>
        <div className={`cardHolder south ${southCard ? 'containsCard' : ''}`}>
          {southCard && <div className={`card-${southCard} card`}/>}
        </div>
        <div className={`cardHolder west ${westCard ? 'containsCard' : ''}`}>
          {westCard && <div className={`card-${westCard} card`}/>}
        </div>
      </div>
    );
  }
}

PlayerCards.propTypes = {
  board: PropTypes.array,
  lastTrickWinner: PropTypes.number,
  className: PropTypes.string
};

export default ActiveBookArea;
