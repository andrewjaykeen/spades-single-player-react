import React from 'react';
import ReactDOM from 'react-dom';
import PlayerCards from './PlayerCards';

import { shallow, mount, render } from 'enzyme';
import { spy } from 'sinon';

function noop() {  return {};  }

it('renders without crashing', () => {
  const div = document.createElement('div');
  const onSelectedCardSpy = noop;
  const cards = [];
  ReactDOM.render(<PlayerCards onSelectedCard={onSelectedCardSpy} cards={cards}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders the players cards', () => {
  const onSelectedCardSpy = spy();
  const cards = [{
    value: 'KS'
  }, {
    value: '5C'
  }];
  const component = mount(<PlayerCards onSelectedCard={onSelectedCardSpy} cards={cards}/>);
  expect(component.find('.playerCards').length).toBe(1);
  expect(component.find('.playerCards .card').length).toBe(2);
});


it('invokes onSelectedCardSpy callback', () => {
  const onSelectedCardSpy = spy();
  const cards = [{
    value: 'KS'
  }, {
    value: '5C'
  }];
  const component = mount(<PlayerCards onSelectedCard={onSelectedCardSpy} cards={cards}/>);
  const fiveOfClubs = component.find('.card-5C');
  expect(fiveOfClubs.length).toBe(1);
  fiveOfClubs.simulate('click');
  expect(onSelectedCardSpy.calledOnce).toBe(true);
  expect(onSelectedCardSpy.calledWith('5C')).toBe(true);
});

