import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {spy} from "sinon";
import { mount } from 'enzyme';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});


it('starts a new game', () => {
  const component = mount(<App/>);
  component.find('.controlPanel button').first().simulate('click');
  expect(component.find('.cardTable').length).toBe(1);
});
